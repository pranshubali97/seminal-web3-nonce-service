"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require('express');
const seminal_web3_service_1 = require("seminal-web3-service");
// eslint-disable-next-line new-cap
const router = express.Router();
const WALLETS_GLOBAL_STATE = seminal_web3_service_1.SeminalUtil.WALLET_ADDRESS_LIST.map((walletItem) => {
    return {
        walletAddress: walletItem.walletAddress,
        web3: walletItem.web3,
        queuedCount: 0,
        processedCount: 0,
        currentNonce: -1,
    };
});
let secToLastWeb3Activity = 0;
const clearNonceThreshold = 600; // 5 min
const checkIntervalSec = 10;
const packageVersion = require('../../package.json')['dependencies']['seminal-web3-service'];
setInterval(() => {
    secToLastWeb3Activity += checkIntervalSec;
    if (secToLastWeb3Activity >= clearNonceThreshold) {
        secToLastWeb3Activity = 0;
        Promise.resolve(initWallets()).then((r) => console.log('Nonce reset timeout and cleared!'));
    }
}, checkIntervalSec * 1000);
const BALANCE_LOWER_BOUND = 10 ** 18;
const getWalletNextNonce = async (walletAddress) => {
    let i = 0;
    for (i = 0; i < WALLETS_GLOBAL_STATE.length; i++) {
        if (WALLETS_GLOBAL_STATE[i].walletAddress === walletAddress) {
            break;
        }
    }
    const nonceValue = await seminal_web3_service_1.SeminalUtil.getAccountNonce(WALLETS_GLOBAL_STATE[i].walletAddress, WALLETS_GLOBAL_STATE[i].web3);
    WALLETS_GLOBAL_STATE[i].currentNonce =
        WALLETS_GLOBAL_STATE[i].currentNonce >= nonceValue ? WALLETS_GLOBAL_STATE[i].currentNonce + 1 :
            nonceValue + 1;
    return WALLETS_GLOBAL_STATE[i].currentNonce - 1;
};
const initWallets = async () => {
    await Promise.all(WALLETS_GLOBAL_STATE.map((item) => seminal_web3_service_1.SeminalUtil.getAccountNonce(item.walletAddress, item.web3)))
        .then((nonceList) => {
        WALLETS_GLOBAL_STATE.forEach((item, index) => {
            item.currentNonce = nonceList[index];
            item.queuedCount = 0;
            item.processedCount = 0;
        });
    });
    console.log(WALLETS_GLOBAL_STATE);
};
initWallets().then((r) => console.log('wallet initialized'));
router.get('/init', async function (req, res, next) {
    await initWallets();
    res.send({
        code: 200,
    });
});
router.get('/apply/:walletAddress', async function (req, res, next) {
    const { walletAddress } = req.params;
    WALLETS_GLOBAL_STATE.forEach((wallet) => {
        if (wallet.walletAddress === walletAddress) {
            wallet.queuedCount += 1;
        }
    });
    try {
        res.send({
            code: 200,
            nonce: await getWalletNextNonce(walletAddress),
        });
    }
    catch (e) {
        console.log(e);
        res.send({
            code: 500,
            nonce: -1,
        });
    }
});
router.get('/requestLeastQueuedWallet', async function (req, res, next) {
    secToLastWeb3Activity = 0;
    try {
        const balanceIsAvailableList = await Promise.all(WALLETS_GLOBAL_STATE.map((wallet) => seminal_web3_service_1.SeminalUtil.getAccountBalance(wallet.walletAddress)))
            .then((r) => r.map((balance) => balance > BALANCE_LOWER_BOUND));
        const queuedCountList = WALLETS_GLOBAL_STATE.map((wallet, index) => {
            if (balanceIsAvailableList[index]) {
                return wallet.queuedCount;
            }
            else {
                return Number.MAX_VALUE;
            }
        });
        const minQueuedCountIndex = queuedCountList.indexOf(Math.min(...queuedCountList));
        WALLETS_GLOBAL_STATE[minQueuedCountIndex].queuedCount += 1;
        const nonce = await getWalletNextNonce(WALLETS_GLOBAL_STATE[minQueuedCountIndex].walletAddress);
        res.send({
            code: 200,
            data: {
                nonce: nonce,
                walletAddress: WALLETS_GLOBAL_STATE[minQueuedCountIndex].walletAddress,
            },
        });
    }
    catch (e) {
        console.log(e);
        res.send({
            code: 500,
            data: {},
        });
    }
});
router.get('/requestLeastProcessedWallet', async function (req, res, next) {
    secToLastWeb3Activity = 0;
    try {
        const balanceIsAvailableList = await Promise.all(WALLETS_GLOBAL_STATE.map((wallet) => seminal_web3_service_1.SeminalUtil.getAccountBalance(wallet.walletAddress)))
            .then((r) => r.map((balance) => balance > BALANCE_LOWER_BOUND));
        const processedCountList = WALLETS_GLOBAL_STATE.map((wallet, index) => {
            if (balanceIsAvailableList[index]) {
                return wallet.processedCount;
            }
            else {
                return Number.MAX_VALUE;
            }
        });
        const minProcessedCountIndex = processedCountList.indexOf(Math.min(...processedCountList));
        WALLETS_GLOBAL_STATE[minProcessedCountIndex].queuedCount += 1;
        WALLETS_GLOBAL_STATE[minProcessedCountIndex].processedCount += 1;
        const nonce = await getWalletNextNonce(WALLETS_GLOBAL_STATE[minProcessedCountIndex].walletAddress);
        res.send({
            code: 200,
            data: {
                nonce: nonce,
                walletAddress: WALLETS_GLOBAL_STATE[minProcessedCountIndex].walletAddress,
            },
        });
    }
    catch (e) {
        console.log(e);
        res.send({
            code: 500,
            data: {},
        });
    }
});
router.get('/removeQueuedWallet/:walletAddress', async function (req, res, next) {
    const { walletAddress } = req.params;
    WALLETS_GLOBAL_STATE.forEach((wallet) => {
        if (wallet.walletAddress === walletAddress && wallet.queuedCount > 0) {
            wallet.queuedCount -= 1;
        }
    });
    res.send({
        code: 200,
        data: {},
    });
});
router.get('/wallets', function (req, res, next) {
    res.send({
        code: 200,
        packageVersion,
        data: WALLETS_GLOBAL_STATE.map((item) => {
            return {
                walletAddress: item.walletAddress,
                hasWeb3: !!item.web3,
                queuedCount: item.queuedCount,
                processedCount: item.processedCount,
                currentNonce: item.currentNonce,
            };
        }),
    });
});
exports.default = router;
